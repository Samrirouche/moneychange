package com.moneychange.fr;

public class Pieces {

	private int ten = 0;
	private int five = 0;
	private int two = 0;
	private int one = 0;
	
	Pieces(int ten, int five, int two, int one){
		this.ten = ten;
		this.five = five;
		this.two = two;
		this.one = one;
	}
	
	int getTen(){
		return this.ten;
	}
	int getFive(){
		return this.five;
	}
	int getTwo(){
		return this.two;
	}
	int getOne(){
		return this.one;
	}
}
