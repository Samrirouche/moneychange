package com.moneychange.fr;

public class Money {
	private static Money instance;
	private int reste = 0;
	private int ten = 0;
	private int five = 0;
	private int two = 0;
	int one = 0;
	
	public static Money getInstance(){
		if(instance == null){
			instance = new Money();
		}
		return instance;
	}
	
	// we declare our methodes here !
	
	Pieces Exchange(int money){
		if(money >= 10){
			ten = money / 10;
			reste = money % 10;
			Exchange(reste);
		}else{
			// check fives
			if(money >= 5){
				five = money / 5;
				reste = money % 5;
				Exchange(reste);
			}else{
				// check two
				if(money >= 2){
					two = money / 2;
					reste = money % 2;
					Exchange(reste);
				}else{
					// check one
					if(money == 1){
						one = 1;
					}
				}
			}
		}
		
		return new Pieces(ten, five, two, one);
	}
}
